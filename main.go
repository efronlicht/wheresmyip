package main

import (
	"errors"
	"fmt"
	"log"
	"net"
	"os"
)

func main() {
	if len(os.Args) != 2 {
		log.Fatal("expected exactly one command line argument")
	}
	ip, err := getIPOfInterfaceByName(os.Args[1])
	if err != nil {
		log.Fatalf("getting IP of %q: %v", os.Args[1], err)
	}
	fmt.Print(ip)
}
func getIPOfInterfaceByName(name string) (net.IP, error) {
	ifc, err := net.InterfaceByName(name)
	if err != nil {
		return nil, fmt.Errorf("looking up interface: %w", err)
	}
	addrs, err := ifc.Addrs()
	if err != nil {
		return nil, fmt.Errorf("getting adresses for interface: %w", err)
	}
	if len(addrs) == 0 {
		return nil, errors.New("no addresses for interface")
	}
	for _, address := range addrs {
		if n, ok := address.(*net.IPNet); ok && !n.IP.IsLoopback() {
			if n.IP.To4() != nil {
				return n.IP, nil
			}
		}
	}
	return nil, errors.New("could not find non-loopback ipv4")

}
